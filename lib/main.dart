import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter!";
String spanishGreeting = "Hola Flutter";
String japanishGreeting = "こんにちはフラッター";
String chineseGreeting = "你好顫振";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  // Widget build(BuildContext context)
  // {
  //   return Container();
  // }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

        home: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.home),
            title: Text("Hello Flutter App"),
            actions: <Widget>[
              IconButton(
                  onPressed: (){
                    setState(() {
                      displayText = displayText == englishGreeting ?
                      spanishGreeting : englishGreeting;
                    });
                  },
                  icon: Icon(Icons.translate)),
              IconButton(
                  onPressed: (){
                    setState(() {
                      displayText = displayText == englishGreeting ?
                      japanishGreeting : englishGreeting;
                    });
                  },
                  icon: Icon(Icons.face_unlock_rounded)),
              IconButton(
                  onPressed: (){
                    setState(() {
                      displayText = displayText == englishGreeting ?
                      chineseGreeting : englishGreeting;
                    });
                  },
                  icon: Icon(Icons.filter_hdr)),
            ],
            // flexibleSpace: ,
          ),
          body: Center(
            child: Text(displayText,
            style: TextStyle(fontSize: 58, fontWeight: FontWeight.bold, color: Colors.blueGrey)),
          )
        ),

    );

  }

}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//
//         home: Scaffold(
//           appBar: AppBar(
//             leading: Icon(Icons.home),
//             title: Text("Hello Flutter App"),
//             actions: <Widget>[
//               IconButton(onPressed: (){}, icon: Icon(Icons.translate)),
//             ],
//             // flexibleSpace: ,
//           ),
//           body: Center(
//             child: Text("Hello Flutter !",
//             style: TextStyle(fontSize: 58, fontWeight: FontWeight.bold, color: Colors.blueGrey)),
//           )
//         ),
//
//       //   home: Center(
//       //     child: Text("Hello Flutter"),
//       //   ),
//
//       // home: SafeArea(
//       //   child: Text("Hello Flutter !"),
//       // ),
//
//     );
//
//   }
// }